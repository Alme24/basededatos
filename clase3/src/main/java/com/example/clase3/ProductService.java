package com.example.clase3;
import java.util.List;

public interface ProductService {
    List<Product> findAllProducts();
}
