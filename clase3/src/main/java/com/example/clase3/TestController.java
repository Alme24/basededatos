package com.example.clase3;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/test")
public class TestController {

    private final PersonService personService;
    private final ProductService productService;
    @Autowired
    public TestController(PersonService personService, ProductService productService) {
        this.personService = personService;
        this.productService = productService;
    }

    @GetMapping("/hello")
    public ResponseEntity<?> helloWorld(){
        return new ResponseEntity<>("<h1>Hola Mundo</h1>", HttpStatus.OK);
    }

    @GetMapping("/persons")
    public ResponseEntity<?> findAllPersons(){
        return new ResponseEntity<>(personService.findAllPersons(), HttpStatus.OK);
    }
    @GetMapping("/products")
    public ResponseEntity<?> findAllProducts(){
        return new ResponseEntity<>(productService.findAllProducts(), HttpStatus.OK);
    }
}
