package com.example.clase3;

import java.util.List;

public interface PersonService {
    List<Person> findAllPersons();
}
